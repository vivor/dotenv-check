const assert = require('chai').assert
const mock = require('mock-fs')
const dotenvCheck = require('../src')

const getMessage = function(config) {
    return dotenvCheck(config)
        .execute()
        .then(result => result === true ? null : `execute() returned ${result}, expected true`)
        .catch(err => err)
}

describe('Dotenv Check module', function () {
    describe('Called with null in sampleEnvFile configuration', function() {
        beforeEach(function() {
            mock({
                '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO'
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: null, checkEnvsExported: false})
            assert.equal(err.constructor.name, "DotenvCheckConfigurationError")
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with an .env file that doesnt exist', function() {
        beforeEach(function() {
            mock({
                '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO',
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.equal(err.constructor.name, "DotenvCheckFileNotFoundError")
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with a custom .env file that doesnt exist', function() {
        beforeEach(function() {
            mock({
                '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO'
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env.custom', sampleEnvFile: '.env.sample'})
            assert.equal(err.constructor.name, "DotenvCheckFileNotFoundError")
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with a custom .env.sample file that doesnt exist', function() {
        beforeEach(function() {
            mock({
                '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO'
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample.custom'})
            assert.equal(err.constructor.name, "DotenvCheckFileNotFoundError")
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with an .env.sample file that doesnt exist', function() {
        beforeEach(function() {
            mock({
                '.env': 'DotenvCheck_VARIABLE_1=HALLO',
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.equal(err.constructor.name, "DotenvCheckFileNotFoundError")
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with an .env.sample that is invalid', function() {
        beforeEach(function() {
            mock({
                '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                '.env.sample': 'DotenvCheck_VARIABLE_1',
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.equal(err.errors[0].constructor.name, "DotenvCheckInvalidDotenvFileStructureError")
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with an .env.sample that has mutliple invalid lines', function() {
        beforeEach(function() {
            mock({
                '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                '.env.sample': 'DotenvCheck_VARIABLE_1\n42=ANSWER',
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.equal(err.errors.length, 2)
            assert.equal(err.errors[0].constructor.name, "DotenvCheckInvalidDotenvFileStructureError")
            assert.equal(err.errors[1].constructor.name, "DotenvCheckInvalidDotenvVariableNameError")
            assert.equal(err.message, [
                "Dotenv file is invalid at line 1",
                "Invalid variable name 42 in line 2. First letter allowed characters: latin letters, _"
            ].join("\n"))
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with an .env that is invalid', function() {
        beforeEach(function() {
            mock({
                '.env': 'DotenvCheck_VARIABLE_1',
                '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO',
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.equal(err.errors[0].constructor.name, "DotenvCheckInvalidDotenvFileStructureError")
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with an .env that has an invalid variable name', function() {
        beforeEach(function() {
            mock({
                '.env': 'DotenvCheck!_VARIABLE_1=hallo',
                '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO',
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.equal(err.errors[0].constructor.name, "DotenvCheckInvalidDotenvVariableNameError")
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with an .env that has an invalid variable name, and the problem exists in the first letter', function() {
        beforeEach(function() {
            mock({
                '.env': '1DotenvCheck_VARIABLE_1=hallo',
                '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO',
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.include(err.errors[0].message, 'First')
            assert.equal(err.errors[0].constructor.name, "DotenvCheckInvalidDotenvVariableNameError")
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with an .env.sample that has an invalid variable name', function() {
        beforeEach(function() {
            mock({
                '.env': 'DotenvCheck_VARIABLE_1=hallo',
                '.env.sample': 'DotenvCheck!_VARIABLE_1=HALLO',
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.equal(err.errors[0].constructor.name, "DotenvCheckInvalidDotenvVariableNameError")
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with an .env.sample that has an invalid variable name, and the problem exists in the first letter', function() {
        beforeEach(function() {
            mock({
                '.env': 'DotenvCheck_VARIABLE_1=hallo',
                '.env.sample': '1DotenvCheck_VARIABLE_1=HALLO',
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.include(err.errors[0].message, 'First')
            assert.equal(err.errors[0].constructor.name, "DotenvCheckInvalidDotenvVariableNameError")
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with an .env that contains duplicate variables', function() {
        describe('Using \\n as the new line delimiter', function() {
            beforeEach(function() {
                mock({
                    '.env': 'DotenvCheck_VARIABLE_1=hallo\nDotenvCheck_VARIABLE_1=hallo1',
                    '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO',
                })
            })
            
            it('should throw an exception', async function () {
                const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
                assert.equal(err.errors[0].constructor.name, "DotenvCheckDotenvDuplicateVariableError")
            })

            afterEach(function() {
                mock.restore()
            })
        })

        describe('Using \\r as the new line delimiter', function() {
            beforeEach(function() {
                mock({
                    '.env': 'DotenvCheck_VARIABLE_1=hallo\rDotenvCheck_VARIABLE_1=hallo1',
                    '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO',
                })
            })
            
            it('should throw an exception', async function () {
                const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
                assert.equal(err.errors[0].constructor.name, "DotenvCheckDotenvDuplicateVariableError")
            })

            afterEach(function() {
                mock.restore()
            })
        })
    })

    describe('Called with an .env that contains duplicate variables', function() {
        describe('Using \\n as the new line delimiter', function() {
            beforeEach(function() {
                mock({
                    '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                    '.env.sample': 'DotenvCheck_VARIABLE_1=hallo\nDotenvCheck_VARIABLE_1=hallo1'
                })
            })
            
            it('should throw an exception', async function () {
                const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
                assert.equal(err.errors[0].constructor.name, "DotenvCheckDotenvDuplicateVariableError")
            })

            afterEach(function() {
                mock.restore()
            })
        })

        describe('Using \\r as the new line delimiter', function() {
            beforeEach(function() {
                mock({
                    '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                    '.env.sample': 'DotenvCheck_VARIABLE_1=hallo\rDotenvCheck_VARIABLE_1=hallo1'
                })
            })
            
            it('should throw an exception', async function () {
                const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
                assert.equal(err.errors[0].constructor.name, "DotenvCheckDotenvDuplicateVariableError")
            })

            afterEach(function() {
                mock.restore()
            })
        })
    })

    describe('Called with an .env that contains a variable that doesnt exist in .env.sample', function() {
        describe('Using \\n as the new line delimiter', function() {
            beforeEach(function() {
                mock({
                    '.env': 'DotenvCheck_VARIABLE_1=HALLO\nDotenvCheck_VARIABLE_2=hallo',
                    '.env.sample': 'DotenvCheck_VARIABLE_1=hallo'
                })
            })
            
            it('should throw an exception', async function () {
                const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
                assert.equal(err.errors[0].constructor.name, "DotenvCheckDotenvFilesMismatchError")
            })

            afterEach(function() {
                mock.restore()
            })
        })

        describe('Using \\r as the new line delimiter', function() {
            beforeEach(function() {
                mock({
                    '.env': 'DotenvCheck_VARIABLE_1=HALLO\rDotenvCheck_VARIABLE_2=hallo',
                    '.env.sample': 'DotenvCheck_VARIABLE_1=hallo'
                })
            })
            
            it('should throw an exception', async function () {
                const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
                assert.equal(err.errors[0].constructor.name, "DotenvCheckDotenvFilesMismatchError")
            })

            afterEach(function() {
                mock.restore()
            })
        })
    })

    describe('Called with an .env that contains multiple variables that don\'t exist in .env.sample', function() {
        beforeEach(function() {
            mock({
                '.env': 'DotenvCheck_VARIABLE_1=hallo\nDotenvCheck_VARIABLE_2=hallo\nDotenvCheck_VARIABLE_3=hallo',
                '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO'
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.equal(err.errors.length, 2)
            assert.equal(err.errors[0].constructor.name, "DotenvCheckDotenvFilesMismatchError")
            assert.equal(err.errors[1].constructor.name, "DotenvCheckDotenvFilesMismatchError")
            assert.equal(err.message, [
                "Variable: DotenvCheck_VARIABLE_2 is missing in the sample .env file",
                "Variable: DotenvCheck_VARIABLE_3 is missing in the sample .env file"
            ].join("\n"))
        })

        it('should ignore keys with prefixes', async function() {
            const err = await getMessage({
                envFile: '.env',
                sampleEnvFile: '.env.sample',
                checkEnvsExported: false,
                ignoreKeysWithPrefixes: ['DotenvCheck']
            })
            assert.notExists(err)
        })

        it('should ignore keys with suffixes', async function() {
            const err = await getMessage({
                envFile: '.env',
                sampleEnvFile: '.env.sample',
                checkEnvsExported: false,
                ignoreKeysWithSuffixes: ['VARIABLE_2']
            })
            assert.equal(err.message, [
                "Variable: DotenvCheck_VARIABLE_3 is missing in the sample .env file"
            ].join("\n"))
        })

        it('should ignore keys', async function() {
            const err = await getMessage({
                envFile: '.env',
                sampleEnvFile: '.env.sample',
                checkEnvsExported: false,
                ignoreKeys: ['DotenvCheck_VARIABLE_3']
            })
            assert.equal(err.message, [
                "Variable: DotenvCheck_VARIABLE_2 is missing in the sample .env file"
            ].join("\n"))
        })

        it('should ignore multiple keys', async function() {
            const err = await getMessage({
                envFile: '.env',
                sampleEnvFile: '.env.sample',
                checkEnvsExported: false,
                ignoreKeys: ['DotenvCheck_VARIABLE_2', 'DotenvCheck_VARIABLE_3']
            })
            assert.notExists(err)
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with an .env.sample that contains a variable that doesnt exist in .env', function() {
        describe('Using \\n as the new line delimiter', function() {
            beforeEach(function() {
                mock({
                    '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                    '.env.sample': 'DotenvCheck_VARIABLE_1=hallo\nDotenvCheck_VARIABLE_2=hallo'
                })
            })
            
            it('should throw an exception', async function () {
                const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
                assert.equal(err.errors[0].constructor.name, "DotenvCheckDotenvFilesMismatchError")
            })
    
            afterEach(function() {
                mock.restore()
            })
        })

        describe('Using \\r as the new line delimiter', function() {
            beforeEach(function() {
                mock({
                    '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                    '.env.sample': 'DotenvCheck_VARIABLE_1=hallo\rDotenvCheck_VARIABLE_2=hallo'
                })
            })
            
            it('should throw an exception', async function () {
                const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
                assert.equal(err.errors[0].constructor.name, "DotenvCheckDotenvFilesMismatchError")
            })
    
            afterEach(function() {
                mock.restore()
            })
        })
    })

    describe('Called with an .env.sample that contains multiple variables that don\'t exist in .env', function() {
        beforeEach(function() {
            mock({
                '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                '.env.sample': 'DotenvCheck_VARIABLE_1=hallo\nDotenvCheck_VARIABLE_2=hallo\nDotenvCheck_VARIABLE_3=hallo'
            })
        })
        
        it('should throw an exception', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.equal(err.errors.length, 2)
            assert.equal(err.errors[0].constructor.name, "DotenvCheckDotenvFilesMismatchError")
            assert.equal(err.errors[1].constructor.name, "DotenvCheckDotenvFilesMismatchError")
            assert.equal(err.message, [
                "Variable: DotenvCheck_VARIABLE_2 is missing in the original .env file",
                "Variable: DotenvCheck_VARIABLE_3 is missing in the original .env file"
            ].join("\n"))
        })

        it('should ignore keys with prefixes', async function() {
            const err = await getMessage({
                envFile: '.env',
                sampleEnvFile: '.env.sample',
                checkEnvsExported: false,
                ignoreKeysWithPrefixes: ['DotenvCheck']
            })
            assert.notExists(err)
        })

        it('should ignore keys with suffixes', async function() {
            const err = await getMessage({
                envFile: '.env',
                sampleEnvFile: '.env.sample',
                checkEnvsExported: false,
                ignoreKeysWithSuffixes: ['VARIABLE_2']
            })
            assert.equal(err.message, [
                "Variable: DotenvCheck_VARIABLE_3 is missing in the original .env file"
            ].join("\n"))
        })

        it('should ignore keys', async function() {
            const err = await getMessage({
                envFile: '.env',
                sampleEnvFile: '.env.sample',
                checkEnvsExported: false,
                ignoreKeys: ['DotenvCheck_VARIABLE_3']
            })
            assert.equal(err.message, [
                "Variable: DotenvCheck_VARIABLE_2 is missing in the original .env file"
            ].join("\n"))
        })

        it('should ignore multiple keys', async function() {
            const err = await getMessage({
                envFile: '.env',
                sampleEnvFile: '.env.sample',
                checkEnvsExported: false,
                ignoreKeys: ['DotenvCheck_VARIABLE_2', 'DotenvCheck_VARIABLE_3']
            })
            assert.notExists(err)
        })

        afterEach(function() {
            mock.restore()
        })
    })

    // We allow empty .env files
    describe('Called with an .env and a .env.sample that are empty', function() {
        beforeEach(function() {
            mock({
                '.env': '',
                '.env.sample': ''
            })
        })
        
        it('should resolve', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.notExists(err)
        })

        afterEach(function() {
            mock.restore()
        })
    })

    describe('Called with a valid .env and .env.sample file', function() {
        beforeEach(function() {
            delete process.env.DotenvCheck_VARIABLE_1
            mock({
                '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO',
            })
        })
               
        it('should resolve if called with no check for exported variables', async function () {
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
            assert.notExists(err)
        })
        
        it('should resolve called with a check for exported variables, and the variables exist', async function (){
            process.env.DotenvCheck_VARIABLE_1 = 'HALLO'
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: true})
            assert.notExists(err)
        })
        
        it('should throw an exception if called with a check for exported variables, and the variables dont exist', async function (){
            delete process.env.DotenvCheck_VARIABLE_1
            const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: true})
            assert.equal(err.errors[0].constructor.name, "DotenvCheckVariableNotExportedError")
        })

        afterEach(function() { 
            mock.restore() 
        })
    })

    describe('Called with a valid .env with empty lines', function() {
        describe('Using \\n as the new line delimiter', function() {
            beforeEach(function() {
                delete process.env.DotenvCheck_VARIABLE_1
                mock({
                    '.env': 'DotenvCheck_VARIABLE_1=HALLO\n\n \n  \n   \n',
                    '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO',
                })
            })
                
            it('should resolve if called with no check for exported variables', async function () {
                const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
                assert.notExists(err)
            })

            afterEach(function() { 
                mock.restore() 
            })
        })

        describe('Using \\r as the new line delimiter', function() {
            beforeEach(function() {
                delete process.env.DotenvCheck_VARIABLE_1
                mock({
                    '.env': 'DotenvCheck_VARIABLE_1=HALLO\r\r \r  \r   \r',
                    '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO',
                })
            })
                
            it('should resolve if called with no check for exported variables', async function () {
                const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
                assert.notExists(err)
            })

            afterEach(function() { 
                mock.restore() 
            })
        })
    })

    describe('Called with a valid .env.sample with empty lines', function() {
        describe('Using \\n as the new line delimiter', function() {
            beforeEach(function() {
                delete process.env.DotenvCheck_VARIABLE_1
                mock({
                    '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                    '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO\n\n \n  \n   \n',
                })
            })
                
            it('should resolve if called with no check for exported variables', async function () {
                const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
                assert.notExists(err)
            })

            afterEach(function() { 
                mock.restore() 
            })
        })

        describe('Using \\r as the new line delimiter', function() {
            beforeEach(function() {
                delete process.env.DotenvCheck_VARIABLE_1
                mock({
                    '.env': 'DotenvCheck_VARIABLE_1=HALLO',
                    '.env.sample': 'DotenvCheck_VARIABLE_1=HALLO\r\r \r  \r   \r',
                })
            })
                
            it('should resolve if called with no check for exported variables', async function () {
                const err = await getMessage({envFile: '.env', sampleEnvFile: '.env.sample', checkEnvsExported: false})
                assert.notExists(err)
            })

            afterEach(function() { 
                mock.restore() 
            })
        })
    })
});
